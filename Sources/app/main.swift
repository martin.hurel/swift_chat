import Kitura

import KituraStencil

import Foundation

let router = Router()

router.all(middleware: [BodyParser(), StaticFileServer(path: "./Public")])

router.add(templateEngine: StencilTemplateEngine())

// La structure de notre Message ou l'on stockera toutes les données
struct Message {
var date : String
var auteur: String
var contenu: String
}

//Par défault la var chat est un tableau vide
var chat : [Message] = []

//On récupère les données envoyer grace au form et on les ajoute dans la structure Message
router.post("/chat"){ request, response, next in
    var auteur = request.body?.asURLEncoded?["auteur"]
    var contenu = request.body?.asURLEncoded?["msg"]
    
    //Get current Date
    let currentDate = Date()

    //Format date to the choosen format and choose datezone
    let formatDate = DateFormatter()
    formatDate.dateFormat = "d MMM yyyy HH:mm:ss"
    formatDate.timeZone = TimeZone(abbreviation: "GMT+01")


    var m = Message(date: formatDate.string(from: currentDate), auteur: (auteur ?? ""), contenu : (contenu ?? ""))
    chat.insert(m, at: 0)
    try response.redirect("/chat")
}



//On va afficher tous les messages 
router.get("/chat") { request, response, next in
    //On regarde si on a un message, sinon on le précise à l'utilisateur
    if(chat.count == 0){
        try response.render("Accueil_chat.stencil", context: ["rien" : "Aucun message pour l'instant"])
        next()
    }
    //On a un message on envoie donc simplement le tableau
    else{
        try response.render("Accueil_chat.stencil", context: ["message" : chat])
        next()
    }   
}

Kitura.addHTTPServer(onPort: 8080, with: router)
Kitura.run()



