# App Chat Swift

![Image description](http://mhurel.eemi.tech/Frame.png)

# Description

Application de chat réaliser en SWIFT avec affichage de l'heure de publication des messages

Application créer dans le cadre d'un exercice de SWIFT 

# Cloner et mise en place du projet 

`$ git clone https://gitlab.com/martin.hurel/swift_chat.git`

`$ cd swift-chat`

`$ swift run`

# Auteurs

Martin **HUREL**, Quentin **LUBACK**, Gregoire **BOUDSOCQ**